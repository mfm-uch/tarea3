\documentclass[../main.tex]{subfiles}

\begin{document}

\begin{enumerate}[a)]
\item Los vectores coordenadas son:
  $$\vec e_u = \frac{\p \vec r}{\p u } = \hat i + \hat j + 2v\,\hat k$$
  $$\vec e_v = \frac{\p \vec r}{\p v } = \hat i - \hat j + 2u\,\hat k$$
  $$\vec e_w = \frac{\p \vec r}{\p w } = \hat k$$
  Verificamos la ortogonalidad:
  $$\vec e_u \cdot \vec e_v = (1 -1 + 4uv) = 4uv$$
  $$\vec e_u \cdot \vec e_w = 2v$$
  $$\vec e_v \cdot \vec e_w = 2u$$
  Se observa que los vectores coordenada no son ortogonales.\\
  Los factores de escala son:
  $$h_u = \vert \vec e_u \vert = \sqrt{2 + 4v^2}$$
  $$h_v = \vert \vec e_v \vert = \sqrt{2 + 4u^2}$$
  $$h_w = \vert \vec e_w \vert = 1 $$
  por lo que solo $\vec e_w$ es unitario.
\item En primer lugar dejamos $u,v$ y $w$ en función de $x,y$ y $z$
  $$ u = \frac{x+y}{2}  $$
  $$ v = \frac{x-y}{2}  $$
  $$ w = z -\frac{1}{2}(x^2-y^2)  $$
  Los vectores bases duales son:
  $$ \vec{e^u} = \nabla u = \nabla\left(\frac{x+y}{2}\right) = \frac{1}{2}\hat i + \frac{1}{2}\hat j  $$
  $$ \vec{e^v} = \nabla v = \nabla\left(\frac{x-y}{2}\right) = \frac{1}{2}\hat i - \frac{1}{2}\hat j  $$
  $$ \vec{e^w} = \nabla w = \nabla\left(z -\frac{1}{2}(x^2-y^2)\right) = -x\hat i + y\hat j  + \hat k$$
\item En primer lugar expresamos los vectores $\hat i, \hat j$ y $\hat k$ en función de $\vec e_u, \vec e_v$ y $\vec e_w$.
  $$\hat i =  \frac{1}{2}\vec e_u + \frac{1}{2}\vec e_v - (u+v)\vec e_w$$
  $$\hat j =  \frac{1}{2}\vec e_u - \frac{1}{2}\vec e_v + (u-v)\vec e_w$$
  $$\hat k = \vec e_w$$
  Luego remplazamos en el vector posición genérico $\vec r = x \hat i + y\hat j + z\hat k$.
  $$\vec r = (u+v)\left(\frac{1}{2}\vec e_u + \frac{1}{2}\vec e_v - (u+v)\vec e_w\right) +(u-v)\left( \frac{1}{2}\vec e_u - \frac{1}{2}\vec e_v + (u-v)\vec e_w  \right) + (2uv+w)\vec e_w$$.
$$\vec r = u\,\vec e_u + v\,\vec e_v + \left[ (u-v)^2-(u+v)^2 \right]\vec e_w + (2uv+w)\vec e_w$$
Notamos que en la diferencia de cuadrados los términos cuadrados se cancelas y solo quedan los términos cruzados.
$$\vec r = u\,\vec e_u + v\,\vec e_v - 4uv\vec e_w + (2uv+w)\vec e_w$$
$$\vec r = u\,\vec e_u + v\,\vec e_v + (w - 3uv)\vec e_w$$
\item El diferencial $d\vec r$ es
  $$ d \vec r = d(u\,\vec e_u) + d(v\,\vec e_v) + d[(w - 3uv)\vec e_w]$$
  $$d\vec r = du\,\vec e_u + u\,d(\vec e_u) + dv\,\vec e_v + v\,d(\vec e_v) + \vec e_w \, d (w - 3uv) + (w - 3uv)\, d(\vec e_w)  $$
  $$d\vec r = du\,\vec e_u + 2u\,dv \,\vec e_w +dv\, \vec e_v + 2v\,du\,\vec e_w + \,(dw - 3u\,dv - 3v\,du) \vec e_w  $$
  $$d\vec r = du\,\vec e_u +dv\, \vec e_v + (dw - u\,dv - v\,du)\vec e_w  $$
  Los diferenciales de área son
  $$dS_{uv}= \vert \vec e_u \times \vec e_v \vert du\,dv = 2\sqrt{2u^2 + 2v^2 +1}du\,dv$$
  $$dS_{wu}= \vert \vec e_w \times \vec e_u \vert dw\,du= \sqrt{2}\,dw\,dv$$
  $$dS_{vw}= \vert \vec e_v \times \vec e_w \vert dv\,du = \sqrt 2 \, dv\,dw $$
  El diferencial de volumen es
  $$dV = (\vec e_w \times \vec e_u)\cdot \vec e_v \, dw\,du\,dv = (1,-1,0)\cdot(1,-1,2u) dw\,dv\,du$$
  $$dV = 2\,dw\,dv\,du$$
\end{enumerate}



\end{document}